public class BinaryToDecimal {
    public static void main(String[] args) {
    int num = 10001;
    int dec = 0;
    int base = 1;
        while (num > 0) {
        int last_digit = num % 10;
        num = num / 10;
        dec = dec + last_digit * base;
        base = base * 2;
        }
        System.out.println(dec);
}
}




